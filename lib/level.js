const mkdirp = require('mkdirp')
const level = require('level')
const liveStream = require('level-live-stream')

function setupLevel (levelPath, callback) {
  mkdirp(levelPath, (err) => {
    if (err) return callback(err)
    level(levelPath, { createIfMissing: true }, callback)
  })
}

function setupLiveStream (db) {
  if (!db) return
  liveStream.install(db)
  return db
}

module.exports = {
  setupLevel,
  setupLiveStream
}
