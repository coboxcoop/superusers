# Superusers

<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

[![standard-readme compliant](https://img.shields.io/badge/standard--readme-OK-green.svg?style=flat-square)](https://github.com/RichardLitt/standard-readme)

## Table of Contents

  - [About](#about)
  - [Install](#install)
  - [Usage](#usage)
  - [API](#api)
  - [Contributing](#contributing)
  - [License](#license)

## About
**CoBox** is an encrypted p2p file system and distributed back-up tool. [README](https://gitlab.com/coboxcoop/readme) provides a map of the project.

`superuser` provides a superuser space which is like a special kind of `@coboxcoop/space`. Rather than managing an archive of files together, it represents the space who manage a cobox hardware seeder, the superuser space, allowing them to control which spaces it replicates.

This is done by sending encrypted messages to [@coboxcoop/seeder](https://gitlab.com/coboxcoop/seeder).

An superuser space simply contains a `@coboxcoop/log` which uses a `kappa-core` and `multifeed` instance, inherited from `@coboxcoop/replicator`, `kappa-view-query` for querying the log, and wraps it all with a cryptographic value encoder so the log is fully encrypted.

# Install

```
npm install @coboxcoop/superusers
```

# Usage

```js
const crypto = require('@coboxcoop/crypto')
const Superusers = require('./')

const storage = tmp()
const address = crypto.address()
const encryptionKey = crypto.encryptionKey()
const identity = crypto.boxKeyPair()
const superusers = Superusers(storage, address, identity, { encryptionKey })

let query = { $filter: { value: { type: 'peer/about' } } }
let stream = superusers.log.read({ query })
stream.on('data', console.log)

superusers.log.publish({ type: 'peer/about', content: { name: 'Magpie' } })
superusers.swarm()
```

## API
See swagger documentation... (we won't have this for a while).

## Contributing

PRs accepted.

Small note: If editing the README, please conform to the [standard-readme](https://github.com/RichardLitt/standard-readme) specification.

# License

[`AGPL-3.0-or-later`](./LICENSE)
