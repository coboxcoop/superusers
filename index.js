const Kappa = require('kappa-core')
const through = require('through2')
const crypto = require('@coboxcoop/crypto')
const debug = require('@coboxcoop/logger')('@coboxcoop/superusers')
const path = require('path')
const maybe = require('call-me-maybe')
const assert = require('assert')
const Log = require('@coboxcoop/log')
const { Replicator } = require('@coboxcoop/replicator')
const { setupLevel } = require('@coboxcoop/replicator/lib/level')
const { loadKey, saveKey } = require('@coboxcoop/keys')
const constants = require('@coboxcoop/constants')
const { encodings } = require('@coboxcoop/schemas')

const PeerAbout = encodings.peer.about
const SpaceAbout = encodings.space.about

const { keyIds } = constants
const { log: LOG_ID } = keyIds

const ENC_KEY = 'encryption_key'

class Superusers extends Replicator {
  /**
   * Create an superuser space
   * @constructor
   */
  constructor (storage, address, identity, opts = {}) {
    var encryptionKey = opts.encryptionKey
    if (opts.encryptionKey) delete opts.encryptionKey

    super(storage, address, identity, opts)

    var key = loadKey(this.storage, ENC_KEY) || encryptionKey
    encryptionKey = crypto.encryptionKey(key)
    assert(crypto.isKey(encryptionKey), `invalid: ${ENC_KEY}`)
    saveKey(this.storage, ENC_KEY, encryptionKey)

    this._deriveKeyPair = opts.deriveKeyPair || randomKeyPair
    this.core = new Kappa()

    var encryptionEncoder = crypto.encoder(encryptionKey, {
      valueEncoding: 'binary',
      nonce: encryptionKey.slice(0, crypto.encoder.NONCEBYTES)
    })

    this._initFeeds({ valueEncoding: encryptionEncoder })
  }

  ready (callback) {
    return maybe(callback, new Promise((resolve, reject) => {
      super.ready((err) => {
        if (err) return reject(err)
        this.open((err) => {
          if (err) return reject(err)
          this.log.ready((err) => {
            if (err) return reject(err)
            this.core.ready((err) => {
              if (err) return reject(err)
              return resolve()
            })
          })
        })
      })
    }))
  }

  createLastSyncStream () {
    const address = hex(this.address)
    return this.lastSyncDb
      .createLiveStream()
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        next(null, {
          type: 'seeder/last-sync',
          address: address,
          data: {
            peerId: msg.key,
            lastSyncAt: msg.value
          }
        })
      }))
  }

  createLogStream () {
    var self = this
    let query = [{ $filter: { value: { timestamp: { $gt: 0 } } } }]
    return this.log
      .read({ query, live: true, old: true })
      .pipe(through.obj(function (msg, _, next) {
        if (msg.sync) return next()
        this.push({
          resourceType: 'ADMIN_SEEDER',
          feedId: msg.key,
          address: hex(self.address),
          data: msg.value
        })
        next()
      }))
  }

  static async onCreate (seeder, { identity }) {
    let peerAbout = {
      type: 'peer/about',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(identity.publicKey),
      content: { name: identity.name }
    }

    let spaceAbout = {
      type: 'space/about',
      version: '1.0.0',
      timestamp: Date.now(),
      author: hex(identity.publicKey),
      content: { name: seeder.name }
    }

    await seeder.log.publish(peerAbout, { valueEncoding: PeerAbout })
    await seeder.log.publish(spaceAbout, { valueEncoding: SpaceAbout })
    return true
  }

  // ------------------------------ PRIVATE FUNCTIONS ------------------------------

  _open (callback) {
    super._open((err) => {
      if (err) return callback(err)

      this.logDb = setupLevel(path.join(this.storage, 'views', 'log'))

      this.log = Log({
        _id: this._id,
        core: this.core,
        feeds: this.feeds,
        muxer: this.muxer,
        keyPair: this._deriveKeyPair(LOG_ID, this.address),
        db: this.logDb
      })

      return callback()
    })
  }

  _closeIndexes (callback) {
    this.core.close(callback)
  }

  _close (callback) {
    super._close(callback)
  }
}

function randomKeyPair (id, context) {
  return crypto.keyPair(crypto.masterKey(), id, context)
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  return buf
}

module.exports = (storage, address, identity, opts) => new Superusers(storage, address, identity, opts)
module.exports.Superusers = Superusers
