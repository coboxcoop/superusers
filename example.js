const crypto = require('@coboxcoop/crypto')
const Superusers = require('./')
const tmpdir = require('tmp').dirSync
const mkdirp = require('mkdirp')
const { encodings } = require('@coboxcoop/schemas')

const PeerAbout = encodings.peer.about

function tmp () {
  var path = '.'+tmpdir().name
  mkdirp.sync(path)
  return path
}

function hex (buf) {
  if (Buffer.isBuffer(buf)) return buf.toString('hex')
  else return buf
}

async function Application () {
  const storage = tmp()
  const address = crypto.address()
  const encryptionKey = crypto.encryptionKey()
  const identity = crypto.boxKeyPair()
  const superusers = Superusers(storage, address, { encryptionKey, identity })

  await superusers.ready()

  let query = [{ $filter: { value: { type: 'peer/about', timestamp: { gt: 0 } } } }]

  superusers.log.read({ query }).on('data', console.log)

  await superusers.log.publish({
    type: 'peer/about',
    version: '1.0.0',
    author: hex(identity.publicKey),
    timestamp: Date.now(),
    content: { name: 'magpie' }
  }, { valueEncoding: PeerAbout })

  superusers.swarm()
}

Application()
