const { describe } = require('tape-plus')
const crypto = require('@coboxcoop/crypto')
const path = require('path')
const fs = require('fs')
const mkdirp = require('mkdirp')
const randomWords = require('random-words')
const sinon = require('sinon')
const proxyquire = require('proxyquire')
const Kappa = require('kappa-core')
const collect = require('collect-stream')
const { encodings } = require('@coboxcoop/schemas')
const Announce = encodings.command.announce
const Hide = encodings.command.hide
const { tmp, cleanup } = require('./util')
const Superusers = require('../')

describe('@coboxcoop/superusers: Superusers', (context) => {
  let sandbox

  context.beforeEach((c) => {
    sandbox = sinon.createSandbox()
  })

  context.afterEach((c) => {
    sandbox.restore()
  })

  context('constructor() - generates a new encryption key', async (assert, next) => {
    var storage = tmp(),
      address = crypto.address(),
      name = randomWords(1).pop(),
      identity = crypto.boxKeyPair()


    var spy = sandbox.spy(crypto)
    var ProxySuperusers = proxyquire('../', { '@coboxcoop/crypto': spy })
    var superusers = ProxySuperusers(storage, address, { publicKey: identity.publicKey, name })

    assert.ok(superusers, 'superusers created')
    assert.same(superusers.address, Buffer.from(address, 'hex'), 'has address')
    assert.same(superusers.identity.name, name, 'has name')
    assert.ok(superusers.core instanceof Kappa, 'has a kappa-core')
    assert.ok(spy.encryptionKey.calledOnce, 'calls crypto.encryptionKey()')
    assert.ok(spy.encoder.calledOnce, 'uses encryption_key in encoder')

    superusers.ready(() => {
      assert.ok(superusers.log, 'has a log')
      cleanup(storage, next)
    })
  })

  context('constructor() - loads encryption_key from path', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var encryptionKey = crypto.encryptionKey()
    var keyPath = path.join(storage, address.toString('hex'), 'encryption_key')

    var identity = crypto.boxKeyPair()

    mkdirp.sync(path.dirname(keyPath))
    fs.writeFileSync(keyPath, encryptionKey, { mode: fs.constants.S_IRUSR })

    var loadStub = sandbox.stub().returns(encryptionKey)
    var saveStub = sandbox.stub().returns(true)

    var ProxySuperusers = proxyquire('../', { '@coboxcoop/keys': {
      loadKey: loadStub,
      saveKey: saveStub
    }})

    var superusers = ProxySuperusers(storage, address, identity)

    assert.ok(loadStub.calledWith(superusers.storage, 'encryption_key'), 'key is loaded')
    assert.ok(saveStub.calledWith(superusers.storage, 'encryption_key', encryptionKey), 'key is saved')

    cleanup(storage, next)
  })

  context('constructor() - saves encryption_key given as argument', (assert, next) => {
    var storage = tmp()
    var address = crypto.address()
    var encryptionKey = crypto.encryptionKey()
    var identity = crypto.boxKeyPair()
    var superusers = Superusers(storage, address, identity, { encryptionKey })
    var key = fs.readFileSync(path.join(superusers.storage, 'encryption_key'))

    assert.ok(superusers, 'superusers loaded')
    assert.same(superusers.address, address, 'same address')
    assert.same(key.toString('hex'), encryptionKey.toString('hex'), 'encryption_key stored correctly')

    cleanup(storage, next)
  })

  context('broadcast', async (assert, next) => {
    var storage = tmp(),
      address = crypto.address(),
      encryptionKey = crypto.encryptionKey(),
      count = 0,
      identity = crypto.boxKeyPair()

    var superusers = Superusers(storage, address, identity, { encryptionKey })

    await superusers.ready()

    await superusers.log.publish({
      type: 'command/announce',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey.toString('hex')
    }, { valueEncoding: Announce })

    await superusers.log.publish({
      type: 'command/hide',
      version: '1.0.0',
      timestamp: Date.now(),
      author: identity.publicKey.toString('hex')
    }, { valueEncoding: Hide })

    await superusers.log.ready()

    let data = await superusers.log.query({
      $filter: {
        value: {
          type: { $in: ['command/announce', 'command/hide'] },
          timestamp: { $gt: 0 }
        }
      }
    })

    assert.same(data.length, 2, 'gets two messages')
    cleanup(storage, next)
  })
})
